# galeria

Proyecto de App realizada con Vuejs, Firebase y Bootstrap.
La finalidad de la app es ser un repositorio de imagenes, para poder subir imagenes solo es necesario loguearse con alguno de los servicios habilitados para ello.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
