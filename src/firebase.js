import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/storage'

const firebaseConfig = {
  apiKey: 'AIzaSyATwadgqL7dvvAeU5URGM-2Ra-rP-jmE24',
  authDomain: 'galeria-14614.firebaseapp.com',
  databaseURL: 'https://galeria-14614.firebaseio.com',
  projectId: 'galeria-14614',
  storageBucket: 'galeria-14614.appspot.com',
  messagingSenderId: '671077474458',
  appId: '1:671077474458:web:228eaa04c31fdf41c7bf23'
}

firebase.initializeApp(firebaseConfig)

export const ProviderGoogle = firebase.auth.GoogleAuthProvider
export const auth = firebase.auth()
export const storage = firebase.storage()
