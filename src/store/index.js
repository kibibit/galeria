import Vue from 'vue'
import Vuex from 'vuex'

import user from './user'
import fotos from './fotos'

import { auth } from '../firebase'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
    checkAuth ({ commit }) {
      auth.onAuthStateChanged((user) => {
        if (user) {
          commit('user/setUser', user)
        } else {
          commit('user/setUser', null)
        }
      })
    }
  },
  modules: {
    user,
    fotos
  }
})

export default store

// Carga Inicial
store.dispatch('checkAuth')
