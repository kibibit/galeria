import { storage } from '../firebase'

const state = {
  carpetas: [],
  archivos: []
}

const mutations = {
  setCarpetas (state, payload) {
    state.carpetas.push(payload)
  },
  setItems (state, payload) {
    state.archivos.push(payload)
  },
  actualizarUrlItem (state, { index, urlimage }) {
    state.archivos[index].url = urlimage
  }
}

const actions = {
  async Listar ({ commit }, userId) {
    const ref = storage.ref().child(userId)
    const list = await ref.list()
    list.prefixes.forEach((folderRef) => {
      commit('setCarpetas', {
        path: folderRef.fullPath,
        name: folderRef.name
      })
    })
    list.items.forEach((itemsRef) => {
      commit('setItems', {
        path: itemsRef.fullPath,
        name: itemsRef.name
      })
    })
  },
  async GetURL ({ state, commit }, path) {
    const ref = storage.ref(path)
    console.log(ref)
    const urlimage = await ref.getDownloadURL()
    const index = state.archivos.findIndex(foto => foto.path === path)
    commit('actualizarUrlItem', { index, urlimage })
    return urlimage
  },
  async SubirImg ({ commit }, { file, path2folder }) {
    const ref = storage.ref(path2folder)
    const imageRef = ref.child(file.name)
    await imageRef.put(file)
    const urlimage = await imageRef.getDownloadURL()
    commit('setItems', {
      path: imageRef.fullPath,
      name: imageRef.name,
      url: urlimage
    })
    return urlimage
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
