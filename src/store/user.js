import { ProviderGoogle, auth } from '../firebase'

const state = {
  user: null
}

const mutations = {
  setUser (state, payload) {
    state.user = payload
  }
}

const actions = {
  getCurrentUser () {
    return new Promise((resolve, reject) => {
      const unsubscribe = auth.onAuthStateChanged(
        user => {
          unsubscribe()
          resolve(user)
        },
        () => {
          // eslint-disable-next-line prefer-promise-reject-errors
          reject()
        }
      )
    })
  },
  async updateProfile ({ commit }, { name, email, password }) {
    const user = auth.currentUser
    if (name) {
      await user.updateProfile({
        displayName: name
      })
    }
    if (email) {
      await user.updateEmail(email)
    }
    if (password) {
      await user.updatePassword(password)
    }
    commit('setUser', user)
  },
  async LoginGoogle ({ commit }) {
    const provider = new ProviderGoogle()
    await auth.signInWithPopup(provider)
    commit('setUser', auth.currentUser)
  },
  async LoginEmail ({ commit }, { email, password }) {
    await auth.signInWithEmailAndPassword(email, password)
    commit('setUser', auth.currentUser)
  },
  async SignIn ({ commit }, { name, email, password }) {
    await auth.createUserWithEmailAndPassword(email, password)
    const user = auth.currentUser
    await user.updateProfile({
      displayName: name
    })
    commit('setUser', user)
  },
  async Logout ({ commit }) {
    try {
      await auth.signOut()
      commit('setUser', null)
    } catch (error) {
      console.error(error.message)
    }
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
